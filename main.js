window.onload = myFunction;
var count = 1;
var maxHeight = 900;
var maxWidth = 800;
var right = document.getElementById('right');
var left = document.getElementById('left');

function myFunction() {
    for (var i = 0; i < count; i++) {
        var randX = Math.floor(Math.random() * 901);
        var randY = Math.floor(Math.random() * 801);

        createImage(randX, randY, no, left);
        createImage(randX, randY, no, right);
    }
    var randX = Math.floor(Math.random() * 901);
    var randY = Math.floor(Math.random() * 801);

    createImage(randX, randY, yes, left);
}

function createImage(randX, randY, yesNo, leftRight) {
    var x = document.createElement("IMG");
    x.setAttribute("src", "./thief.png");
    x.setAttribute("width", "41");
    x.setAttribute("height", "57");
    x.setAttribute("style", "position:absolute");
    x.addEventListener("click", yesNo);
    x.style.marginLeft = randX + "px";
    x.style.marginTop = randY + "px";
    leftRight.appendChild(x);
}

function yes() {
    count = count + 1;
    right.innerHTML = "";
    left.innerHTML = "";
    myFunction();
}

function no() {
    count = 1;
    right.innerHTML = "";
    left.innerHTML = "";
    myFunction();
}
