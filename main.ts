window.onload = myFunction;
var count : number = 1;
var maxHeight : number = 900;
var maxWidth : number = 800;
var right : HTMLElement = document.getElementById('right');
var left : HTMLElement = document.getElementById('left');

function myFunction()
{
  for(var i = 0; i < count; i++)
  {
    var randX : Number = Math.floor(Math.random() * 901);
    var randY : Number = Math.floor(Math.random() * 801);

    createImage(randX, randY, no, left);
    createImage(randX, randY, no, right);
  }
  var randX : Number = Math.floor(Math.random() * 901);
  var randY : Number = Math.floor(Math.random() * 801);

  createImage(randX, randY, yes, left);
}

function createImage(randX, randY, yesNo, leftRight)
{
  var x : HTMLElement = document.createElement("IMG");
  x.setAttribute("src", "./thief.png");
  x.setAttribute("width", "41");
  x.setAttribute("height", "57");
  x.setAttribute("style","position:absolute");
  x.addEventListener("click",yesNo);
  x.style.marginLeft = randX + "px";
  x.style.marginTop = randY + "px";
  leftRight.appendChild(x);
}

function yes()
{
  count = count + 1;
  right.innerHTML = "";
  left.innerHTML = "";
  myFunction();
}

function no()
{
  count = 1;
  right.innerHTML = "";
  left.innerHTML = "";
  myFunction();
}
